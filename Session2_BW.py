# Advanced Python Programming 
# Name: Bo Wan
# Session 2

from swampy.TurtleWorld import *
import math
# Chapter4_4.3Exercises_and_Exercise1

# No.1
def square(t):
    for number in range(0,4):
        fd(t,100)
        lt(t)

def square1(t,length):
    for number in range(0,4):
        fd(t,length)
        lt(t)

def polygon(t,length,n):
    """ Draws a polygon with n sides
        t is a turtle instance
        length is just the length of the sides 
        of the polygon
    """
    for number in range(n):
        fd(t,length)
        lt(t,360/n)

def circle(t,r):
    """
    Draws a cicrle with a given radius r
    t is a turtle instance
    n is the circumference of the circle
    times is the number of the segments drawn
    """
    n = 2 * math.pi * r
    times = 50
    for number in range(0,times):
        fd(t,n/times)
        lt(t,360/times)

def arc1(t,r,angle=360):
    n = 2 * math.pi * r
    times = 50
    length = n * (angle / 360) / times
    for number in range(0,times):
        fd(t,length)
        lt(t,angle / times)

# No.2
# Since my circle function does not have nested function in it, 
# so stack diagram shown below is based on the solution code from the 
#online book thinkpython.

# bob = Turtle()
# radius = 30
# circle(bob, radius)

# <module>  bob ---> Turtle()
#           radius ---> 30

#    arc    t ---> Turtle() 
#           r ---> 30
#           angle ---> 360
#           arc_length ---> 2 * math.pi * r * abs(angle) / 360
#           n ---> int(arc_length / 4) + 1
#           step_length ---> arc_length / n
#           step_angle ---> float(angle) / n

#  polyline t ---> Turtle()
#           n ---> int(arc_length / 4) + 1
#           length ---> arc_length / n
#           angle ---> float(angle) / n



# Chapter4_Exercise2
def draw_pattern(t,r_arc,angle_arc):
    for i in range(0,2):  
        arc(t,r_arc,angle_arc)
        lt(t,180 - angle_arc) # make sure bob return to initial facing postion

def drawing(t,r_arc,angle_arc,n):
    angle_pattern = 360 / n

    for i in range(0,n):
        draw_pattern(t,r_arc,angle_arc)
        lt(t,angle_pattern) # make sure that bob is facing the next postion

def move_bob(angle, length):
    pu(bob)
    lt(bob,angle)
    fd(bob,length)
    pd(bob)
    rt(bob,angle)



# Chapter4_Exercise3
def triangle(t,angle,radius):
    angle1 = angle / 2
    side = 2 * (math.sin(math.pi * (angle1 / 180)) * radius) 
    rt(t,angle1)
    fd(t,radius)
    lt(t,angle1 + 90)
    fd(t,side)
    lt(t , angle1 + 90)
    fd(t,radius)
    lt(t, 180 - angle1)

def polygon_tri(t,n,radius):
    angle = 360 / n
    for i in range(0,n):
        triangle(t,angle,radius)
        lt(t,angle)



# Chapter4_Exercise4
def backToOrigin(t, n, angle = 0):
    lt(t, angle)
    fd(t, n)
    bk(t, n)
    rt(t, angle)

def hover(t, n, angle = 0):
    lt(t,angle)
    pu(t)
    fd(t, n)
    pd(t)
    rt(t,angle)

def notBackToOrigin(t, n, angle = 0):
    lt(t, angle)
    fd(t, n)
    rt(t, angle)


def beam(t, n, unit):
    hover(t, n * unit, 90)
    backToOrigin(t, n)
    hover(t, -n * unit, 90)


def hangman(t, n, unit):
    notBackToOrigin(t, n * unit,90)
    backToOrigin(t, n)
    rt(t)
    fd(t, n * unit)
    lt(t)

def diagonal(t, x, y):
    angle = (math.atan2(y, x) / math.pi) * 180
    print (angle)
    length = math.sqrt(x ** 2 + y ** 2)
    backToOrigin(t,length,angle)

def vshape(t, n, unit):
    diagonal(t, -n / 2, unit * n)
    diagonal(t, n / 2, unit * n)

def bump(t, n, unit):
    notBackToOrigin(t, n * unit,90)
    arc1(t, n / 2, 180)
    lt(t,90)
    fd(t, n * unit + n)
    lt(t, 90)


def draw_a(t, n):
    diagonal(t, n, 2 * n)
    hover(t, n, 90)
    backToOrigin(t, 2 * n)
    hover(t, -n, 90)
    hover(t, 2 * n)
    diagonal(t, -n, 2 * n)

def draw_b(t, n):
    bump(t, n, 1)
    bump(t, n, 0)
    hover(t, n / 2)

def draw_c(t, n):
    hangman(t, n, 2)
    fd(t, n)

def draw_d(t, n):
    bump(t, 2 * n, 0)
    hover(t, n)

def draw_e(t, n):
    hangman(t, n, 2)
    hangman(t, n, 1)
    fd(t,n)

def draw_f(t, n):
    hangman(t, n, 2)
    hangman(t, n, 1)

def draw_g(t, n):
    hangman(t, n, 2)
    fd(t, n / 2)
    beam(t, n / 2, 2)
    fd(t, n / 2)
    backToOrigin(t,n,90)

def draw_h(t, n):
    backToOrigin(t,2 * n,90)
    hangman(t, n, 1)
    hover(t, n)
    backToOrigin(t,2 * n,90)

def draw_i(t, n):
    beam(t, n, 2)
    fd(t, n / 2)
    backToOrigin(t,2 * n,90)
    fd(t, n / 2)

def draw_j(t, n):
    beam(t, n, 2)
    arc1(t, n / 2, 90)
    fd(t, 3 * n / 2)
    hover(t, -2 * n)
    rt(t)
    hover(t, n / 2)

def draw_k(t, n):
    backToOrigin(t, 2 * n, 90)
    notBackToOrigin(t, n, 90)
    rt(t,90)
    vshape(t, 2 * n, 1 / 2)
    fd(t, n)
    lt(t)
    hover(t, n)


def draw_l(t, n):
    backToOrigin(t, 2 * n, 90)
    fd(t, n)

def draw_m(t, n):
    backToOrigin(t, 2 * n, 90)
    hover(t, n/2)
    diagonal(t, -n / 2, 2 * n)
    diagonal(t, n / 2, 2 * n)
    hover(t, n/2) 
    backToOrigin(t, 2 * n, 90)  

def draw_n(t, n):
    backToOrigin(t, 2 * n, 90)
    hover(t, n)
    diagonal(t, -n, 2 * n)
    backToOrigin(t, 2 * n, 90)

def draw_o(t, n):
    hover(t, n)
    arc1(t, n)
    hover(t, n)

def draw_p(t, n):
    bump(t, n, 1)
    hover(t, n / 2)

def draw_q(t, n):
    hover(t, n)
    arc1(t, n)
    hover(t, n)
    diagonal(t, -n / 2, n)

def draw_r(t, n):
    bump(t, n, 1)
    hover(t, n / 2)
    diagonal(t, -n / 2, n)

def draw_s(t, n):
    fd(t, n / 2)
    arc1(t, n / 2, 180)
    lt(t,180)
    arc1(t, n / 2, -180)
    bk(t, n / 2)
    hover(t, -2 * n, -90)
    lt(t,180)

def draw_t(t, n):
    beam(t, n, 2)
    hover(t, n / 2)
    backToOrigin(t, 2 * n, 90)
    hover(t, n / 2)


def draw_u(t, n):
    backToOrigin(t, 2 * n, 90)
    fd(t, n)
    backToOrigin(t, 2 * n, 90)

def draw_v(t, n):
    hover(t, n / 2)
    vshape(t, n, 2)
    hover(t, n / 2)

def draw_w(t, n):
    draw_v(t, n)
    draw_v(t, n)

def draw_x(t, n):
    diagonal(t, n, 2 * n)
    hover(t, n)
    diagonal(t, -n, 2 * n)


def draw_y(t, n):
    hover(t, n / 2)
    notBackToOrigin(t, n, 90)
    vshape(t, n, 1)
    hover(t,n, -90)
    fd(t, n /2)

def draw_z(t, n):
    beam(t, n, 2)
    diagonal(t, n, 2 * n)
    fd(t, n)



# Chapter4_Exercise5

def draw_spiral(t, n, a=0.1, b=2,theta = 0):
    while n > 0:
        for i in range(n):
            r = a + b * theta
            circumference1 = 2 * math.pi * r
            length = circumference1 / n
            fd(t, length)
            lt(t, 360 / n)
            theta += 1 / (a + b * theta)
        n -= 1
        draw_spiral(t,n,a,b,theta)
        return





# Chapter_5_Exercise_1
def print_n(s, n):
    if n <= 0:
        return
    print (s)
    print_n(s, n-1)

# print_n("Hello",2)

# Stack Diagram for function print_n("Hello",2)

# <module> 
# print_n    s ---> "Hello" n ---> 2
# print_n    s ---> "Hello" n ---> 1
# print_n    s ---> "Hello" n ---> 0


# Chapter_5_Exercise_2

def do_n(f,n):
    if n <= 0:
        return 
    else:
        f(n)
        return do_n(f,n-1)

def f(n):
    print (n)


# Chapter_5_Exercise_3

def check_fermat(a,b,c,n):
    if n <= 2:
        print ("No, that doesn't work.")
    else:
        if (a ** n + b ** n) == c ** n:
            print ("Holy smokes, Fermat was Wrong!")
        else:
            check_fermat(a,b,c,n-1)


def check(x,y):
    try:
        x = int(x)
    except ValueError:
        print ("Please put an integer")
        x = input("Please put the number and press enter: ")
        x = check(x,y)
    if x > y:
        return x
    else:
        print ("Please put a integer is bigger than",y)
        x = input("Please put the number and press enter: ")
        x = check(x,y)
        return x

def fermat_prompt():
    a = input("Put the number for 'a' and press enter: ")
    a = check(a,0)
    b = input("Put the number for 'b' and press enter: ")
    b = check(b,0)
    c = input("Put the number for 'c' and press enter: ")
    c = check(c,0)
    n = input("Put the number for 'n' and press enter: ")
    n = check(n,2)
    check_fermat(a,b,c,n)


# Chapter_5_Exercise_4

def is_triangle(a,b,c,n=3):
    if n <= 0:
        print("Yes")
        return 
    else:
        if a + b < c:
            print("No")
            return
        else:
            is_triangle(b,c,a,n-1)

def is_triangle_prompt():
    a = input("Put the number for 'a' and press enter: ")
    a = check(a,0)
    b = input("Put the number for 'b' and press enter: ")
    b = check(b,0)
    c = input("Put the number for 'c' and press enter: ")
    c = check(c,0)
    is_triangle(a,b,c)


# Chapter_5_Exercise_6
 
def koch(t,length):
    if length < 3:
        fd(t,length)
    else:
        new_length = length / 3
        koch(t,new_length)
        lt(t,60)
        koch(t,new_length)
        rt(t,120)
        koch(t,new_length)
        lt(t,60)
        koch(t,new_length)
        
def snowflake(t,length,n):
    for i in range(n):
        koch(t,length)
        rt(t,360/n)




# Chapter_6_Exercise_1

def compare(x,y):
    if x > y:
        return 1
    elif x == y:
        return 0
    else:
        return -1


# Chapter_6_Exercise_2

# Step 1
# def hypotenuse(x,y):
#     print ("correct")
#     return 

# Step 2
# def hypotenuse(x,y):
#     x1 = x ** 2
#     y1 = y ** 2
#     print (x1,y1)
#     return 

# Step 3
# def hypotenuse(x,y)
#     x1 = x ** 2
#     y1 = y ** 2
#     z1 = x1 + y1
#     print(z1)
#     return 

# Step 4
# def hypotenuse(x,y)
#     x1 = x ** 2
#     y1 = y ** 2
#     z1 = x1 + y1
#     z = math.sqrt(z1)
#     print (z)
#     return z

# Step 5
def hypotenuse(x,y):
    x1 = x ** 2
    y1 = y ** 2
    z1 = x1 + y1
    z = math.sqrt(z1)
    return z


# Chapter_6_Exercise_3

def is_between(x,y,z):
    return (x <= y and y <= z)


# Chapter_6_Exercise_4

def b(z):
    prod = a(z, z)
    print (z, prod)
    return prod

def a(x, y):
    x = x + 1
    return x * y

def c(x, y, z):
    total = x + y + z
    square = b(total)**2
    return square

x = 1
y = x + 1
print (c(x, y+3, x+y))

# <module>   x ---> 1 y---> 2
#    c       x ---> 1 y---> 5 z---> 3 total ---> 9 square ---> 8100
#    b       z ---> 9 prod --- 90
#    a       x ---> 10 y ---> 9


# Chapter_6_Exercise_5

def ack(m,n):
    if m == 0:
        return (n + 1)
    else:
        if n == 0:
            return ack(m-1,1)
        else:
            return ack(m-1,ack(m,n-1))


# Chapter_6_Exercise_6

def first(word):
    return word[0]

def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]
def is_palindrome(s):
    if len(s) <= 1:
        return True
    else:
        if first(s) != last(s):
            return False
        else:
            return is_palindrome(middle(s))


# Chapter_6_Exercise_7

def is_power(a,b):
    if a == 1:
        return True
    elif a / b < 1:
        return False
    elif a / b == 1:
        return True
    return is_power(a/b,b)
    
print (is_power(0,2))


# Chapter_6_Exercise_8

def gcd(a,b):
    if b == 0:
        return a
    else:
        r = a % b
        return gcd(b,r)

        

if __name__ == '__main__':
    world = TurtleWorld()

    # create and position the turtle
    size = 20
    bob = Turtle()
    bob.delay = 0.01 

    draw_spiral(bob, n=50)
    die(bob)

    wait_for_user()


